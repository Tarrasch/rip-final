#ifndef __NEWTONIAN_PHYSICS_H_
#define __NEWTONIAN_PHYSICS_H_

#include <iostream>
#include <stdlib.h>
#include <list>
#include <Eigen/LU>
#include <Eigen/Core>
#define PRINT(x) std::cout << #x << " = " << x << std::endl;

using namespace std;
using namespace Eigen;

std::list< VectorXd > projectileMotion(VectorXd pos, VectorXd vel);
std::list< VectorXd > projectileMotionWind(VectorXd pos, VectorXd vel, VectorXd wind);
std::list< VectorXd > projectileMotionRand(VectorXd pos, VectorXd vel, int rand);
std::list< VectorXd > projectileMotionWindRand(VectorXd pos, VectorXd vel, VectorXd wind, int randMaxAcc);

#endif
