#include <iostream>
#include <stdlib.h>
#include <Eigen/LU>
#include "newtonianPhysics.h"
#define PRINT(x) std::cout << #x << " = " << x << std::endl;
#define ECHO(x) std::cout << x << std::endl;
#include <time.h>

std::list< VectorXd > projectileMotion(VectorXd pos, VectorXd vel){
  ECHO("START Projectile Motion");
  VectorXd acc;
  double const g = -9.82/10.0;
  acc.resize(3);
  acc << 0, 0, g;

  std::list<VectorXd> res;
  double dt = 0.05;

  do {
    res.push_back(pos);
    pos += vel*dt + acc*dt*dt/2.0;
    vel += acc*dt;
  } while (pos[2] > 0);
  ECHO("DONE Projectile Motion");
  return res;
}

std::list< VectorXd > projectileMotionWind(VectorXd pos, VectorXd vel, VectorXd wind){
  ECHO("START Windy Projectile Motion");
  VectorXd acc;
  double const g = -9.82/10.0;
  acc.resize(3);
  acc << wind[0], wind[1], wind[2]+g;

  std::list<VectorXd> res;
  double dt = 0.05;

  do {
    res.push_back(pos);
    pos += vel*dt + acc*dt*dt/2.0;
    vel += acc*dt;
  } while (pos[2] > 0);
  ECHO("DONE Projectile Motion");
  return res;
}

std::list< VectorXd > projectileMotionRand(VectorXd pos, VectorXd vel, int randMaxAcc){
  ECHO("START Random Projectile Motion"); 
  /* initialize random seed: */
  srand ( time(NULL) );
  
  VectorXd acc;
  acc.resize(3);  
  
  double const g = -9.82/10.0;
  acc << 0, 0, g;
  std::list<VectorXd> res;
  double dt = 0.05;
  do {
    VectorXd tempAcc;
	tempAcc.resize(3);
	float randNum = (float)rand()/((float)RAND_MAX/(randMaxAcc)) - (float)randMaxAcc/2;
	PRINT(randNum);
	tempAcc << randNum + acc[0], randNum + acc[1], randNum + acc[2];
	PRINT(tempAcc);
    res.push_back(pos);
    pos += vel*dt + tempAcc*dt*dt/2.0;
    vel += tempAcc*dt;
  } while (pos[2] > 0);
  ECHO("DONE Projectile Motion");
  return res;
}

std::list< VectorXd > projectileMotionWindRand(VectorXd pos, VectorXd vel, VectorXd wind, int randMaxAcc){
  ECHO("START Random Projectile Motion"); 
  /* initialize random seed: */
  srand ( time(NULL) );
  
  VectorXd acc;
  acc.resize(3);  
  
  double const g = -9.82/10.0;
  acc << wind[0], wind[1], wind[2]+g;
  std::list<VectorXd> res;
  double dt = 0.05;
  do {
    VectorXd tempAcc;
	tempAcc.resize(3);
	float randNum = (float)rand()/((float)RAND_MAX/(randMaxAcc)) - (float)randMaxAcc/2;
	PRINT(randNum);
	tempAcc << randNum + acc[0], randNum + acc[1], randNum + acc[2];
	PRINT(tempAcc);
    res.push_back(pos);
    pos += vel*dt + tempAcc*dt*dt/2.0;
    vel += tempAcc*dt;
  } while (pos[2] > 0);
  ECHO("DONE Projectile Motion");
  return res;
}